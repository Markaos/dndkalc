import org.gradle.api.tasks.bundling.Jar

plugins {
    `java-library`
    `maven-publish`
    kotlin("jvm") version "1.3.50"
}

group = "cz.markaos.dndkalc"
version = "0.1.1"

repositories {
    mavenCentral()
    google()
    jcenter()
}

buildscript {
    repositories {
        mavenCentral()
        google()
        jcenter()
    }
}

dependencies {
    "implementation"(kotlin("stdlib"))
}

val sourcesJar by tasks.registering(Jar::class) {
    classifier = "sources"
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar.get())
        }

        repositories {
            maven {
                url = uri("https://mymavenrepo.com/repo/7ewoh29VYvcfHJGEKTQJ/")
                credentials {
                    username = project.property("privateMavenUser") as? String
                    password = project.property("privateMavenPassword") as? String
                }
            }
        }
    }
}
