package cz.markaos.dndkalc

import cz.markaos.dndkalc.stats.StatRanges
import cz.markaos.dndkalc.stats.Range

enum class Class (val humanName: String, val baseHealth: Int, val healthBonus: String, val statRanges: StatRanges) {
    Warrior (
            "válečník",
            10,
            "1d10",
            StatRanges(Range(13, 18), Range(0, 0), Range(13, 18), Range(0, 0), Range(0, 0))
    ),
    Ranger (
            "hraničář",
            8,
            "1d6+2",
            StatRanges(Range(11, 16), Range(0, 0), Range(0, 0), Range(12, 17), Range(0, 0))
    ),
    Alchemist (
            "alchymista",
            7,
            "1d6+1",
            StatRanges(Range(0, 0), Range(13, 18), Range(12, 17), Range(0, 0), Range(0, 0))
    ),
    Apprentice (
            "kouzelník",
            6,
            "1d6",
            StatRanges(Range(0, 0), Range(0, 0), Range(0, 0), Range(14, 19), Range(13, 18))
    ),
    Thief (
            "zloděj",
            6,
            "1d6",
            StatRanges(Range(0, 0), Range(14, 19), Range(0, 0), Range(0, 0), Range(12, 17))
    ),
}