package cz.markaos.dndkalc

import cz.markaos.dndkalc.items.Item
import cz.markaos.dndkalc.stats.Stats

class Character(val name: String,
                val race: Race,
                val clazz: Class,
                val money: Money,
                val inventory: List<Item> = emptyList(),
                val stats: Stats) {

}