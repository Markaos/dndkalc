package cz.markaos.dndkalc

data class Money (val gp: Int, val sp: Int) {
    constructor(silver: Int) : this(silver / 100, silver % 100)

    override fun toString(): String = "${if(gp > 0) "$gp zl." else ""} ${if(sp > 0) " $sp st." else ""}".trim()

    fun toSilverPieces(): Int = gp * 100 + sp

    fun withInflation(inflation: Double): Money = Money((toSilverPieces() * inflation).toInt())
}