package cz.markaos.dndkalc

import cz.markaos.dndkalc.stats.RaceModifiers
import cz.markaos.dndkalc.stats.StatRanges
import cz.markaos.dndkalc.stats.Range

enum class Race (val humanName: String, val ability: String, val modifiers: RaceModifiers, val baseStats: StatRanges) {
    Hobbit (
            "hobit",
            "čich",
            RaceModifiers(-5, +2, 0, -2, +3),
            StatRanges(Range(3, 8), Range(11, 16), Range(8, 13), Range(10, 15), Range(8, 18))
    ),
    Kuduk (
            "kudůk",
            "none",
            RaceModifiers(-3, +1, +1, -2, 0),
            StatRanges(Range(5, 10), Range(10, 15), Range(10, 15), Range(9, 14), Range(7, 12))
    ),
    Dwarf (
            "trpaslík",
            "infravidění",
            RaceModifiers(+1, -2, +3, -3, +2),
            StatRanges(Range(7, 12), Range(7, 12), Range(12, 17), Range(8, 13), Range(7, 12))
    ),
    Elf (
            "elf",
            "none",
            RaceModifiers(0, -2, -4, +2, +2),
            StatRanges(Range(6, 11), Range(10, 15), Range(6, 11), Range(12, 17), Range(8, 18))
    ),
    Human (
            "člověk",
            "none",
            RaceModifiers(0, 0, 0, 0, 0),
            StatRanges(Range(6, 16), Range(9, 14), Range(9, 14), Range(10, 15), Range(2, 17))
    ),
    Barbarian (
            "barbar",
            "none",
            RaceModifiers(+1, -1, +1, 0, -2),
            StatRanges(Range(10, 15), Range(8, 13), Range(11, 16), Range(6, 11), Range(1, 16))
    ),
    Kroll (
            "kroll",
            "ultrasluch",
            RaceModifiers(+3, -4, +3, -6, -5),
            StatRanges(Range(11, 16), Range(5, 10), Range(13, 18), Range(2, 7), Range(1, 11))
    ),
    Ent (
        "ent",
        "zakořenění",
        RaceModifiers(-3, +1, -3, +2, 0),
        StatRanges(Range(3, 8), Range(10, 15), Range(3, 8), Range(8, 18), Range(2, 17))
    ),
}