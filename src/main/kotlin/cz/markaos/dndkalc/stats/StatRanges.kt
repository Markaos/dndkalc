package cz.markaos.dndkalc.stats

data class Range (val min: Int, val max: Int) {
    fun withModifier(modifier: Int): Range = Range(min + modifier, max + modifier)

    fun asDiceRoll(): String {
        val diff = max - min
        return if (diff % 5 == 0) {
            "${min - diff/5}+${diff / 5}d6"
        } else {
            "Nope"
        }
    }

    fun applyDiceRoll(roll: Int): Stat = Stat(min + roll)

    fun empty(): Boolean = min == 0 && max == 0
}

data class StatRanges (val strength: Range, val agility: Range, val constitution: Range, val intelligence: Range, val charisma: Range)