package cz.markaos.dndkalc.stats

data class Stat (val level: Int, val modifier: Int) {
    constructor(level: Int): this(level, levelToModifier(level))

    companion object {
        private fun levelToModifier(level: Int): Int = when {
            level < 2  -> -5
            level < 4  -> -4
            level < 6  -> -3
            level < 8  -> -2
            level < 10 -> -1
            level < 13 ->  0
            level < 15 -> +1
            level < 17 -> +2
            level < 19 -> +3
            level < 21 -> +4
            else       -> +5
        }
    }
}
data class Stats (val strength: Stat, val agility: Stat, val constitution: Stat, val intelligence: Stat, val charisma: Stat)