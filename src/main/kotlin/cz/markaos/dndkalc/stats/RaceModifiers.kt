package cz.markaos.dndkalc.stats

data class RaceModifiers(val strength: Int, val agility: Int, val constitution: Int, val intelligence: Int, val charisma: Int)