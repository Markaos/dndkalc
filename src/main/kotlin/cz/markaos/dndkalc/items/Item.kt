package cz.markaos.dndkalc.items

import cz.markaos.dndkalc.Money

data class Item (val name: String, val price: Money, val weight: Int)

enum class BaseItems (val item: Item) {
    LeatherBag (Item("kožená torna", Money(5, 5), 20)),
    LargeBag (Item("velký vak", Money(0, 8), 12)),
    LittleBag (Item("malý vak", Money(0, 4), 7)),
    WaterSkin (Item("měch na vodu", Money(0, 1), 5)),
    WaterBottle (Item("flakónek", Money(0, 4), 3)),
    Bottle (Item("láhev (půl čtvrtky)", Money(0, 1), 5)),
    Wine (Item("víno (čtvrtka)", Money(0, 4), 20)),
    BeerKeg (Item("soudek piva (5 čtvrtek)", Money(0, 5), 120)),
    Mead (Item("medovina (0,3 čtvrtky)", Money(0, 3), 6)),
    BasicFood (Item("skromné jídlo", Money(2, 3), 150)),
    LuxuryFood (Item("dobré jídlo", Money(5, 5), 200)),
    Dinner (Item("večeře v hostinci", Money(1, 0), 0)),
    Bed (Item("nocleh v hostinci", Money(1, 0), 0)),
    Torches (Item("pochodně (10 ks)", Money(0, 1), 100)),
    BasicLantern (Item("obyčejná lucerna", Money(0, 4), 24)),
    CloseableLantern (Item("lucerna s okenicemi", Money(3, 0), 35)),
    Oil (Item("olej (půl čtvrtky)", Money(0, 5), 10)),
    FlintAndSteel (Item("křesadlo", Money(0, 1), 2)),
    HolyWater (Item("svěcená voda (0,1 čtvrtky)", Money(1, 0), 2)),
    Staff (Item("hůl (1,5 sáhu)", Money(0, 3), 10)),
    Rope (Item("provaz (10 sáhů)", Money(0, 10), 20)),
    Mirror (Item("kovové zrcátko", Money(0, 1), 2)),
    Parchment (Item("pergamen (20x30 coulů)", Money(4, 0), 1)),
    FakeParchment (Item("nepravý pergamen (20x30 coulů)", Money(0, 8), 1)),
    Hammer (Item("kladivo", Money(0, 4), 15)),
    Nails (Item("hřebíky (30 ks)", Money(0, 1), 5)),
    Saw (Item("pila", Money(0, 15), 15)),
    Saddle (Item("sedlo", Money(50, 0), 70)),
    BasicClothes (Item("obyčejné oblečení", Money(8, 0), 40)),
    LuxuryClothes (Item("zdobené oblečení", Money(80, 0), 60)),
    BasicWarmClothes (Item("obyčejné zimní oblečení", Money(20, 0), 80)),
    LuxuryWarmClothes (Item("zdobené zimní oblečení", Money(180, 0), 120)),
    LargeChest (Item("velká truhla (železná)", Money(40, 0), 1500)),
    SmallChest (Item("malá truhla (dřevěná)", Money(5, 0), 50)),
    Mule (Item("mezek", Money(50, 0), 4000)),
    BadHorse (Item("obyčejný kůň", Money(60, 0), 13000)),
    GoodHorse (Item("jezdecký kůň", Money(250, 0), 13000)),
    WarHorse (Item("válečný kůň", Money(1000, 0), 13000)),

}