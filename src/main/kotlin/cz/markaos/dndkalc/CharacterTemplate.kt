package cz.markaos.dndkalc

import cz.markaos.dndkalc.stats.StatRanges
import cz.markaos.dndkalc.stats.Stats
import cz.markaos.dndkalc.items.Item

class CharacterTemplate (val race: Race, val clazz: Class) {

    val statRanges: StatRanges = StatRanges(
            if (!clazz.statRanges.strength.empty())
                clazz.statRanges.strength.withModifier(race.modifiers.strength) else race.baseStats.strength,

            if (!clazz.statRanges.agility.empty())
                clazz.statRanges.agility.withModifier(race.modifiers.agility) else race.baseStats.agility,

            if (!clazz.statRanges.constitution.empty())
                clazz.statRanges.constitution.withModifier(race.modifiers.strength) else race.baseStats.constitution,

            if (!clazz.statRanges.intelligence.empty())
                clazz.statRanges.intelligence.withModifier(race.modifiers.intelligence) else race.baseStats.intelligence,

            if (!clazz.statRanges.charisma.empty())
                clazz.statRanges.charisma.withModifier(race.modifiers.charisma) else race.baseStats.charisma
    )

    fun make(name: String,
             money: Money,
             inventory: List<Item>,
             strengthRoll: Int,
             agilityRoll: Int,
             constitutionRoll: Int,
             intelligenceRoll: Int,
             charismaRoll: Int): Character

            = Character(name, race, clazz, money, inventory,
                Stats(statRanges.strength.applyDiceRoll(strengthRoll),
                      statRanges.agility.applyDiceRoll(agilityRoll),
                      statRanges.constitution.applyDiceRoll(constitutionRoll),
                      statRanges.intelligence.applyDiceRoll(intelligenceRoll),
                      statRanges.charisma.applyDiceRoll(charismaRoll)
                )
              )

}